<?php
defined('BASEPATH') OR exit('No direct script access allowed');




$route['default_controller'] = 'DispatchController';
$route['backend'] = 'DispatchController/backend';


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


$route['module/testservice'] = 'TestController';
$route['module/testservice/loadData'] = 'TestController/loadData';



$route['module/jobcategoryservice/search'] = 'JobCategoryController/Search';
$route['module/jobcategoryservice/add'] = 'JobCategoryController/Add';
$route['module/jobcategoryservice/update'] = 'JobCategoryController/Update';
$route['module/jobcategoryservice/remove'] = 'JobCategoryController/Remove';

$route['module/jobsubcategoryservice/search'] = 'JobSubCategoryController/Search';
$route['module/jobsubcategoryservice/selectcategory'] = 'JobSubCategoryController/SelectCategory';
$route['module/jobsubcategoryservice/add'] = 'JobSubCategoryController/Add';
$route['module/jobsubcategoryservice/remove'] = 'JobSubCategoryController/Remove';
$route['module/jobsubcategoryservice/update'] = 'JobSubCategoryController/Update';

$route['module/accountmanageservice/search'] = 'AccountManageController/Search';
$route['module/accountmanageservice/add'] = 'AccountManageController/Add';
$route['module/accountmanageservice/remove'] = 'AccountManageController/Remove';
$route['module/accountmanageservice/update'] = 'AccountManageController/Update';

$route['module/filefactservice/getfile'] = 'FileFactController/GetFile';
$route['module/filefactservice/upload'] = 'FileFactController/Upload';

$route['module/logservice/getlogs'] = 'LogController/GetLogs';
$route['module/logservice/downloadlog'] = 'LogController/DownloadLog';

$route['module/cliservice/clearfilefact'] = 'Cli/ClearFileFact';
$route['module/cliservice/resetpasswordadmin'] = 'Cli/ResetPasswordAdmin';