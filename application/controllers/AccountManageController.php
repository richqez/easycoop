<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AccountManageController extends CI_Controller {

	
	public function Search(){

		$result =  $this->db->get('accounts')->result();


		echo json_encode($result);
	}


	public function Add(){

		$retrive = json_decode(file_get_contents('php://input'));

		$newItem = [

			"account_std_id" => $retrive->stdid,
			"password" 		 => $retrive->password,
			"firstname" 	 => $retrive->fname,
			"lastname" 		 => $retrive->lname,
			"gender" 		 => $retrive->gender,
			"email" 		 => $retrive->email,
			"phone_no" 		 => $retrive->phoneno

		];


		$this->db->set('account_id', 'UUID()', FALSE);
		$this->db->insert('accounts',$newItem);
		

	}

	public function Update(){

		$retrive = json_decode(file_get_contents('php://input'));

		$updateItem = [

			"account_std_id" => $retrive->stdid ,
			"password" 		 => $retrive->password ,
			"firstname" 	 => $retrive->fname ,
			"lastname" 	 	 => $retrive->lname ,
			"gender" 		 => $retrive->gender ,
			"email" 		 => $retrive->email ,
			"phone_no" 		 => $retrive->phoneno 


		];


		$this->db->where('account_id', $retrive->id);
		$this->db->update('accounts', $updateItem);

		$updatedItem = $this->db->get_where('accounts', ['account_id',$retrive->id])->result();

		echo json_encode($updatedItem);


	}


	public function Remove(){

		$retrive = json_decode(file_get_contents('php://input'));

		$this->db->where('account_id', $retrive->id);
		$this->db->delete('accounts');

	}






}

/* End of file TestController.php */
/* Location: ./application/controllers/TestController.php */