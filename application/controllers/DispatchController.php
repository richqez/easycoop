<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DispatchController extends CI_Controller {

	public function index()
	{
		
		$this->load->view('FrontEnd');

	}


	public function backend(){

		$this->load->view('BackEnd');

	}

}

/* End of file DispatchController.php */
/* Location: ./application/controllers/DispatchController.php */