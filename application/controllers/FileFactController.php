<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FileFactController extends CI_Controller {

	function __construct()
	{
		parent :: __construct();

	}

	public function GetFile()
	{
		$dirName = './uploadtemp/';

		

		$fileFact = $this->input->get('filefactid');

		$result = $this->db->get_where('file_fact', array('file_id' => $fileFact))->first_row();

		$filePath = $dirName .$result->file_name;

		file_put_contents($filePath,$result->file_data);

		header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename="'.basename($filePath).'"');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($filePath));
       	readfile($filePath);

       	unlink($filePath);
        
		//var_dump(base64_encode($result->file_data));
	}


	public function Upload()
	{
		$this->load->helper('file');
		$extFileRule = ['png','pdf','jpg','doc','docx'];

		$dirName = './uploadtemp/';
		$temp = explode(".", $_FILES["file"]["name"]);
		if (in_array(end($temp),$extFileRule)) {

			$newFilename = round(microtime(true)) ;

			$fullPath = $dirName .$newFilename . '.' . end($temp) ;

			move_uploaded_file($_FILES['file']['tmp_name'],$fullPath);


			 $absulotePath  =  substr(__DIR__,0,strlen(__DIR__) - 23 ) . 'uploadtemp\\'  . $newFilename. '.'. end($temp);
			
			$fileInfo = get_file_info($absulotePath);

			$extFile = end($temp);

			$fileFact = [
				'file_name' 	 =>  rand(0,10000) . '-' .$fileInfo['name'],
				'file_size' 	 => $fileInfo['size'],
				'file_mime_type' => $extFile,
				'file_data' 	 => read_file($absulotePath)


			];

			$this->db->set('file_id', 'UUID()', FALSE);
			$this->db->insert('file_fact',$fileFact);

			unlink($absulotePath);


			$this->db->select('MAX(file_id) as id from file_fact');
			echo $this->db->get()->first_row()->id;

		}else{
			echo "Not allowed file type";
		}



		
	}
}
