<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JobCategoryController extends CI_Controller {

	
	public function Search(){

		$result =  $this->db->get('category_jobs')->result();


		echo json_encode($result);
	}


	public function Add(){

		$retrive = json_decode(file_get_contents('php://input'));

		$newItem = [

			"category_job_name" => $retrive->name


		];


		$this->db->set('category_job_id', 'UUID()', FALSE);
		$this->db->insert('category_jobs',$newItem);
		

	}


	public function Update(){

		$retrive = json_decode(file_get_contents('php://input'));

		$updateItem = [

			"category_job_name" => $retrive->name


		];


		$this->db->where('category_job_id', $retrive->id);
		$this->db->update('category_jobs', $updateItem);

		$updatedItem = $this->db->get_where('category_jobs', ['category_job_id',$retrive->id])->result();

		echo json_encode($updatedItem);


	}


	public function Remove(){

		$retrive = json_decode(file_get_contents('php://input'));

		$this->db->where('category_job_id', $retrive->id);
		$this->db->delete('category_jobs');
		
	}






}

/* End of file TestController.php */
/* Location: ./application/controllers/TestController.php */