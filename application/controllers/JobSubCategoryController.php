<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JobSubCategoryController extends CI_Controller {

	
	public function Search(){

		$this->db->select('*');
		$this->db->from('category_sub_jobs');
		$this->db->join('category_jobs', 'category_jobs.category_job_id = category_sub_jobs.category_job_id');
		
		$result =  $this->db->get()->result();


		echo json_encode($result);
	}

	public function SelectCategory(){

		$result = $this->db->get('category_jobs')->result();

		echo json_encode($result);
	}

	public function Add(){

		$retrive = json_decode(file_get_contents('php://input'));

		$newItem = [

			"category_job_id" 		=> $retrive->job_id,
			"category_sub_job_name" => $retrive->name


		];


		$this->db->set('category_sub_job_id', 'UUID()', FALSE);
		$this->db->insert('category_sub_jobs',$newItem);
		

	}

	public function Update(){

		$retrive = json_decode(file_get_contents('php://input'));

		$updateItem = [

			"category_job_id" => $retrive->job_id ,
			"category_sub_job_name" => $retrive->name


		];


		$this->db->where('category_sub_job_id', $retrive->id);
		$this->db->update('category_sub_jobs', $updateItem);

		$updatedItem = $this->db->get_where('category_sub_jobs', ['category_sub_job_id',$retrive->id])->result();

		echo json_encode($updatedItem);


	}




	public function Remove(){

		$retrive = json_decode(file_get_contents('php://input'));

		$this->db->where('category_sub_job_id', $retrive->id);
		$this->db->delete('category_sub_jobs');

	}






}

/* End of file TestController.php */
/* Location: ./application/controllers/TestController.php */