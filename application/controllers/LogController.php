<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LogController extends CI_Controller {

	function __construct()
	{
		parent :: __construct();

	}

	public function GetLogs()
	{
		$this->load->helper('file');

		$absulotePath  =  substr(__DIR__,0,strlen(__DIR__) - 12 )."\logs";

		$logs = get_dir_file_info($absulotePath);

		$listOfLog  = [];

		foreach ($logs as $key => $value) {
			if ($key == 'index.html') {
				continue;
			}else{
				array_push($listOfLog,$key);
			}

		}
		var_dump($listOfLog);
	}

	public function DownloadLog()
	{
		$filePath  =  substr(__DIR__,0,strlen(__DIR__) - 12 ).'\logs' . '\\' . $this->input->get('log') ;
		if (file_exists($filePath)) {
			
			header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
		    header('Content-Disposition: attachment; filename="'.basename($filePath).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($filePath));
	       	readfile($filePath);
		}else{
			echo "file not found";
		}

	}

}