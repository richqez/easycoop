<!DOCTYPE html>
<html >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">

    <title></title>


        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/bower_components/angular-bootstrap/ui-bootstrap-csp.css">
          <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/bower_components/angular-bootstrap/ui-bootstrap-csp.css">
      
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/bower_components/angular-ui-select/dist/select.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/bower_components/angular-loading-bar/build/loading-bar.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/bower_components/angular-ui-router-anim-in-out/css/anim-in-out.css">

        

        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/ngbackend/custom.css">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url() ?>/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>/ngbackend/js/vendor.bundle.js"></script>
        <script src="<?php echo base_url() ?>/bower_components/angular-bootstrap/ui-bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
        <script src="<?php echo base_url() ?>/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
        <script src="<?php echo base_url() ?>/bower_components/angularUtils-pagination/dirPagination.js"></script>
        <script src="<?php echo base_url() ?>/bower_components/angular-ui-select/dist/select.min.js"></script>
        <script src="<?php echo base_url() ?>/bower_components/angular-sanitize/angular-sanitize.min.js"></script>
        <script src="<?php echo base_url() ?>/bower_components/angular-ui-validate/dist/validate.min.js"></script>
        <script src="<?php echo base_url() ?>/bower_components/angular-loading-bar/build/loading-bar.min.js"></script>
        <script src="<?php echo base_url() ?>/bower_components/angular-animate/angular-animate.min.js"></script>
        <script src="<?php echo base_url() ?>/bower_components/angular-confirm-modal/angular-confirm.js"></script>
        <script src="<?php echo base_url() ?>/bower_components/angular-ui-router-anim-in-out/anim-in-out.js"></script>
        <script src="<?php echo base_url() ?>/ngbackend/js/app.bundle.js"></script>
</head>
<body ng-app="easyCoop-backend">
    <div id="loading-bar-container"></div>
    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
                        Start Bootstrap
                    </a>
                </li>
                <li>
                    <a  ui-sref="jobcategory">Master JobCategory</a>
                </li>
                <li>
                    <a  ui-sref="jobsubcategory">Master JobSubCategory</a>
                </li>
                <li>
                    <a  ui-sref="accountmanage">Master AccountManage</a>
                </li>
                <li>
                    <a href="#">Shortcuts</a>
                </li>
                <li>
                    <a href="#">Overview</a>
                </li>
                <li>
                    <a href="#">Events</a>
                </li>
                <li>
                    <a href="#">About</a>
                </li>
                <li>
                    <a href="#">Services</a>
                </li>
                <li>
                    <a href="#">Contact</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div ui-view class="anim-in-out anim-fade" data-anim-speed="700"></div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    
  
</body>
</html>