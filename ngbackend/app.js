angular.module('easyCoop-backend', 
	[
	
	'ui.router'
	,'chieffancypants.loadingBar'
	,'angular-loading-bar'
	,'ngAnimate'
	,'angularUtils.directives.dirPagination'
	,'ui.select'
	,'ngSanitize'
	,'ui.validate'
	,'anim-in-out'
	,'angular-confirm'
	,'ui.bootstrap.modal'
	,'ui.bootstrap.tpls'
	,'ui.bootstrap.alert'

	

	])


	.constant('Config', {
	    "ServiceUrl": '../../easycoop/module/',
	    "ViewsUrl": '../../easycoop/ngbackend/app/views/'
	});










require('./app/controllers');

require('./app/config');
