'use strict';

var angular = require('angular');

angular.module('easyCoop-backend')
	.config(require('./route.config'))
	.config(require('./pagination.config'))
	.config(require('./plugin.config'));


