


'use strict';


function PaginationConfig(paginationTemplateProvider,Config){

	paginationTemplateProvider.setPath(Config.ViewsUrl+ 'dirPagination.tpl.html');

}


module.exports = PaginationConfig;