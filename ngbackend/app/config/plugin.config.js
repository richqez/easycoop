


'use strict';


function PluginConfig(cfpLoadingBarProvider,Config){

 cfpLoadingBarProvider.includeSpinner = true;
 cfpLoadingBarProvider.loadingBarInterceptor = true;
 cfpLoadingBarProvider.latencyThreshold = 50;
	
/*
	cfpLoadingBarProvider.includeSpinner = true;
	cfpLoadingBarProvider.includeBar = true;
	cfpLoadingBarProvider.latencyThreshold = 500;*/
	//cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
    //cfpLoadingBarProvider.spinnerTemplate = '<div><span class="fa fa-spinner">Custom Loading Message...</div>';
}

module.exports = PluginConfig;