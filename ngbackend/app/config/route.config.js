
'use strict';



function RouteConfig($stateProvider, $urlRouterProvider,Config){


	$urlRouterProvider.otherwise("/test");

	$stateProvider
		.state('test',{
				url:'/test',
				templateUrl:  Config.ViewsUrl + 'default.html',
				controller: 'TestController',
				controllerAs: 'vm'
			})
		.state('jobcategory',{
				url:'/jobcategory',
				templateUrl:  Config.ViewsUrl + 'view.jobcategory.html',
				controller: 'JobCategoryController',
				controllerAs: 'vm'
			})
		.state('jobsubcategory',{
				url:'/jobsubcategory',
				templateUrl:  Config.ViewsUrl + 'view.jobsubcategory.html',
				controller: 'JobSubCategoryController',
				controllerAs: 'vm'
			})
		.state('accountmanage',{
				url:'/accountmanage',
				templateUrl:  Config.ViewsUrl + 'view.accountmanage.html',
				controller: 'AccountManageController',
				controllerAs: 'vm'
			});
		




}


module.exports = RouteConfig;