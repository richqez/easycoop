'use strict';




function AccountManageController($http,Config,$scope){
	
	
	//hard Config 
	$http.defaults.headers.post["Content-Type"] = "application/json";



	var vm = this ;

	vm.keyword = "";

	vm.IsEditMode = false ;

	vm.account =  null;

	vm.dtPageSize = 5 ;

	vm.dtCurrentPage = 1 ;

	vm.accountCollection = null;

	vm.alerts = [];

	vm.IsViewDetailMode = false;
	vm.viewDetail = null;


	vm.switchToViewDetail = (item)=>{

		vm.IsEditMode = false ;
		vm.IsViewDetailMode = true;
		vm.viewDetail = item ;
		
	}
	

	vm.swtichToEditMode = function (){

		vm.IsEditMode = true ;
		vm.IsViewDetailMode = false;

	}

	vm.swtichToViewMode = function(){

		vm.IsEditMode = false ;
		vm.account = null;
		vm.IsViewDetailMode = false;
	}


	vm.remove = (item)=>{

		$http.post(Config.ServiceUrl + "accountmanageservice/remove" , { "id" :  item.account_id } )
		.then(
			(response)=>{

				vm.alerts.push({type:'success' ,msg: 'Success Account : '+item.firstname +' '+item.lastname+' has removed .'});
				vm.fetchFromServer();
				vm.swtichToViewMode();

				

		},  (response)=>{

				vm.alerts.push({type:'danger',msg: 'Error : '+ response });
		});

	}

	vm.closeAlert = (index)=>{

		vm.alerts.splice(index, 1);

	}

	vm.update = (item)=>{
		
		vm.swtichToEditMode();

		vm.account = {
			"id" : item.account_id,
			"stdid" : item.account_std_id,
			"password" : item.password,
			"fname"  : item.firstname,
			"lname"  : item.lastname,
			"gender"  : item.gender,
			"email"  : item.email,
			"phoneno"  : item.phone_no

		};

	
	}

	vm.save = ()=>{

		if(vm.account.id){

			$http.post(Config.ServiceUrl + "accountmanageservice/update" , vm.account )
			.then(
				(response)=>{
					vm.alerts.push({type:'success',msg: 'Success Account : has been updated .'});
					vm.swtichToViewMode();
					vm.fetchFromServer();

			},  (response)=>{

					vm.alerts.push({type:'danger',msg: 'Error : '+ response });

			});

		}else{

			$http.post(Config.ServiceUrl + "accountmanageservice/add" , vm.account )
			.then(
				(response)=>{
					vm.alerts.push({type:'success',msg: 'Success Account : '+ vm.account.fname +' '+vm.account.lname +' has been added .'});
					vm.swtichToViewMode();
					vm.fetchFromServer();

			},  (response)=>{

					vm.alerts.push({type:'danger',msg: 'Error : '+ response });

			});
		}
		
	}



	vm.fetchFromServer =  ()=>{

		$http.get(Config.ServiceUrl + "accountmanageservice/search")
			.then(
				(response)=>{

				vm.accountCollection = response.data;


			},	(response)=>{

				alert(response.statusText);

			});


	}

	vm.fetchFromServer();

}

module.exports = AccountManageController;