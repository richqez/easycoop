'use strict';

var angular = require('angular');

angular.module('easyCoop-backend')
	.controller('TestController', require('./test.controller'))
	.controller('JobCategoryController', require('./jobcategory.controller'))
	.controller('JobSubCategoryController', require('./jobsubcategory.controller'))
	.controller('AccountManageController', require('./accountmanage.controller'));