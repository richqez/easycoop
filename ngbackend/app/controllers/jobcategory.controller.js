'use strict';




function JobCategoryController($http,Config,$scope){
	
	
	//hard Config 
	$http.defaults.headers.post["Content-Type"] = "application/json";



	var vm = this ;

	vm.keyword = "";

	vm.IsEditMode = false ;

	vm.jobcategory =  null;

	vm.dtPageSize = 5 ;

	vm.dtCurrentPage = 1 ;

	vm.jobcategoryCollection = null;

	vm.alerts = [];


	vm.IsViewDetailMode = false;
	vm.viewDetail = null;


	vm.switchToViewDetail = (item)=>{

		vm.IsEditMode = false ;
		vm.IsViewDetailMode = true;
		vm.viewDetail = item ;
		
	}


	vm.swtichToEditMode =  ()=>{

		vm.IsEditMode = true ;
		vm.IsViewDetailMode = false;

	}

	vm.swtichToViewMode = () =>{

		vm.IsEditMode = false ;
		vm.jobcategory = null;
		vm.IsViewDetailMode = false;
	}


	vm.remove = (item)=>{
		
		$http.post(Config.ServiceUrl + "jobcategoryservice/remove" , { "id" :  item.category_job_id } )
		.then(
			(response)=>{


				vm.alerts.push({type:'success' ,msg: 'Success JobCategory : '+item.category_job_name +' has removed .'});
				vm.fetchFromServer();

				

		},  (response)=>{

				vm.alerts.push({type:'danger',msg: 'Error : '+ response });
				
		});

	}

	vm.closeAlert = (index)=>{
		vm.alerts.splice(index, 1);
	}


	vm.update = (item)=>{
		vm.jobcategory = {
			"id" : item.category_job_id,
			"name"  : item.category_job_name
		};
		vm.swtichToEditMode();


	}


	vm.save = ()=>{


		if (vm.jobcategory.id) {

			$http.post(Config.ServiceUrl + "jobcategoryservice/update" , vm.jobcategory )
			.then(
				(response)=>{


					vm.alerts.push({type:'success',msg: 'Success JobCategory : updated.'});
					vm.swtichToViewMode();
					vm.fetchFromServer();

			},  (response)=>{

				vm.alerts.push({type:'danger',msg: 'Error : '+ response });

			});

		}else{

			$http.post(Config.ServiceUrl + "jobcategoryservice/add" , vm.jobcategory )
			.then(
				(response)=>{

					vm.alerts.push({type:'success',msg: 'Success JobCategory : '+ vm.jobcategory.name +' has been added .'});
					vm.swtichToViewMode();
					vm.fetchFromServer();

			},  (response)=>{

				vm.alerts.push({type:'danger',msg: 'Error : '+ response });

			});
		}

		
	}



	vm.fetchFromServer =  ()=>{

		$http.get(Config.ServiceUrl + "jobcategoryservice/search")
			.then(
				(response)=>{

				vm.jobcategoryCollection = response.data;


			},	(response)=>{

				vm.alerts.push({type:'danger',msg: 'Error : '+ response });

			});
	}



	vm.fetchFromServer();
	



}

module.exports = JobCategoryController;