'use strict';




function JobSubCategoryController($http,Config,$scope,cfpLoadingBar){
	
	
	//hard Config 
	$http.defaults.headers.post["Content-Type"] = "application/json";



	let vm = this ;

	vm.keyword = "";

	vm.IsEditMode = false ;

	vm.jobsubcategory =  null;

	vm.dtPageSize = 5 ;

	vm.dtCurrentPage = 1 ;

	vm.jobsubcategoryCollection = null;

	vm.jobcategorySelectList = null;

	vm.jobcategorySelected = null ;

	vm.alerts = [];	



	vm.IsViewDetailMode = false;
	vm.viewDetail = null;




	vm.switchToViewDetail = (item)=>{

		vm.IsEditMode = false ;
		vm.IsViewDetailMode = true;
		vm.viewDetail = item ;
		
	}

	vm.swtichToEditMode =  ()=>{

		vm.IsEditMode = true ;
		vm.IsViewDetailMode = false;

	}

	vm.swtichToViewMode = () =>{

		vm.IsEditMode = false ;
		vm.jobsubcategory = null;
		vm.IsViewDetailMode = false;
	}


	vm.remove = (item)=>{

		$http.post(Config.ServiceUrl + "jobsubcategoryservice/remove" , { "id" :  item.category_sub_job_id } )
		.then(
			(response)=>{

				vm.alerts.push({type:'success' ,msg: 'Success JobSubCategory : '+item.category_sub_job_name +' has removed .'});
				vm.fetchFromServer();
				vm.swtichToViewMode();

				

		},  (response)=>{

				vm.alerts.push({type:'danger',msg: 'Error : '+ response });

		});

	}

	vm.closeAlert = (index)=>{

		vm.alerts.splice(index, 1);

	}

	vm.update = (item)=>{
		
		vm.swtichToEditMode();


		angular.forEach(vm.jobcategorySelectList,(jobcategory)=>{

			if (jobcategory.category_job_id == item.category_job_id) {
				vm.jobcategorySelected = jobcategory;
			} 

		});


		vm.jobsubcategory = {
			"id" : item.category_sub_job_id,
			"name"  : item.category_sub_job_name
		};

		

	}


	vm.save = ()=>{

		if(vm.jobsubcategory.id){

			vm.jobsubcategory.job_id = vm.jobcategorySelected.category_job_id;

			$http.post(Config.ServiceUrl + "jobsubcategoryservice/update" , vm.jobsubcategory )
			.then(
				(response)=>{

					vm.alerts.push({type:'success',msg: 'Success JobSubCategory : has been updated .'});
					vm.swtichToViewMode();
					vm.fetchFromServer();

			},  (response)=>{

				vm.alerts.push({type:'danger',msg: 'Error : '+ response });

			});


		}else{

			vm.jobsubcategory.job_id = vm.jobcategorySelected.category_job_id;

			$http.post(Config.ServiceUrl + "jobsubcategoryservice/add" , vm.jobsubcategory )
			.then(
				(response)=>{

					vm.alerts.push({type:'success',msg: 'Success JobSubCategory : '+ vm.jobsubcategory.name +' has been added .'});
					vm.swtichToViewMode();
					vm.fetchFromServer();

			},  (response)=>{

				vm.alerts.push({type:'danger',msg: 'Error : '+ response });

			});

		}
		

	}



	vm.loadJobCategoty = ()=>{

		$http.get(Config.ServiceUrl + "jobsubcategoryservice/selectcategory")
			.then(
				(response)=>{
				
				vm.jobcategorySelectList = response.data;			

			},	(response)=>{

				vm.alerts.push({type:'danger',msg: 'Error : '+ response });

			});
	}


	vm.fetchFromServer =  ()=>{

		 //cfpLoadingBar.start();

		$http.get(Config.ServiceUrl + "jobsubcategoryservice/search")
			.then(
				(response)=>{

				vm.jobsubcategoryCollection = response.data;


			},	(response)=>{

				vm.alerts.push({type:'danger',msg: 'Error : '+ response });

			});

	}





	vm.fetchFromServer();
	vm.loadJobCategoty();




}

module.exports = JobSubCategoryController;