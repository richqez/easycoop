webpackJsonp([0],[
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	angular.module('easyCoop-backend', 
		[
		
		'ui.router'
		,'chieffancypants.loadingBar'
		,'angular-loading-bar'
		,'ngAnimate'
		,'angularUtils.directives.dirPagination'
		,'ui.select'
		,'ngSanitize'
		,'ui.validate'
		,'anim-in-out'
		,'angular-confirm'
		,'ui.bootstrap.modal'
		,'ui.bootstrap.tpls'
		,'ui.bootstrap.alert'

		

		])


		.constant('Config', {
		    "ServiceUrl": '../../easycoop/module/',
		    "ViewsUrl": '../../easycoop/ngbackend/app/views/'
		});










	__webpack_require__(1);

	__webpack_require__(8);


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var angular = __webpack_require__(2);

	angular.module('easyCoop-backend')
		.controller('TestController', __webpack_require__(4))
		.controller('JobCategoryController', __webpack_require__(5))
		.controller('JobSubCategoryController', __webpack_require__(6))
		.controller('AccountManageController', __webpack_require__(7));

/***/ },
/* 2 */,
/* 3 */,
/* 4 */
/***/ function(module, exports) {

	'use strict';




	function TestController($http,Config,$scope){
		
		

		var vm = this ;

		vm.data = [] ;

		

		loadData();




		function loadData(){


			$http.get(Config.ServiceUrl + "testservice/loadData")
				.then(
					(response)=>{

					vm.data = response.data;


				},	(response)=>{

					alert(response.statusText);

				});

		}



	}

	module.exports = TestController;

/***/ },
/* 5 */
/***/ function(module, exports) {

	'use strict';




	function JobCategoryController($http,Config,$scope){
		
		
		//hard Config 
		$http.defaults.headers.post["Content-Type"] = "application/json";



		var vm = this ;

		vm.keyword = "";

		vm.IsEditMode = false ;

		vm.jobcategory =  null;

		vm.dtPageSize = 5 ;

		vm.dtCurrentPage = 1 ;

		vm.jobcategoryCollection = null;

		vm.alerts = [];


		vm.IsViewDetailMode = false;
		vm.viewDetail = null;


		vm.switchToViewDetail = (item)=>{

			vm.IsEditMode = false ;
			vm.IsViewDetailMode = true;
			vm.viewDetail = item ;
			
		}


		vm.swtichToEditMode =  ()=>{

			vm.IsEditMode = true ;
			vm.IsViewDetailMode = false;

		}

		vm.swtichToViewMode = () =>{

			vm.IsEditMode = false ;
			vm.jobcategory = null;
			vm.IsViewDetailMode = false;
		}


		vm.remove = (item)=>{
			
			$http.post(Config.ServiceUrl + "jobcategoryservice/remove" , { "id" :  item.category_job_id } )
			.then(
				(response)=>{


					vm.alerts.push({type:'success' ,msg: 'Success JobCategory : '+item.category_job_name +' has removed .'});
					vm.fetchFromServer();

					

			},  (response)=>{

					vm.alerts.push({type:'danger',msg: 'Error : '+ response });
					
			});

		}

		vm.closeAlert = (index)=>{
			vm.alerts.splice(index, 1);
		}


		vm.update = (item)=>{
			vm.jobcategory = {
				"id" : item.category_job_id,
				"name"  : item.category_job_name
			};
			vm.swtichToEditMode();


		}


		vm.save = ()=>{


			if (vm.jobcategory.id) {

				$http.post(Config.ServiceUrl + "jobcategoryservice/update" , vm.jobcategory )
				.then(
					(response)=>{


						vm.alerts.push({type:'success',msg: 'Success JobCategory : updated.'});
						vm.swtichToViewMode();
						vm.fetchFromServer();

				},  (response)=>{

					vm.alerts.push({type:'danger',msg: 'Error : '+ response });

				});

			}else{

				$http.post(Config.ServiceUrl + "jobcategoryservice/add" , vm.jobcategory )
				.then(
					(response)=>{

						vm.alerts.push({type:'success',msg: 'Success JobCategory : '+ vm.jobcategory.name +' has been added .'});
						vm.swtichToViewMode();
						vm.fetchFromServer();

				},  (response)=>{

					vm.alerts.push({type:'danger',msg: 'Error : '+ response });

				});
			}

			
		}



		vm.fetchFromServer =  ()=>{

			$http.get(Config.ServiceUrl + "jobcategoryservice/search")
				.then(
					(response)=>{

					vm.jobcategoryCollection = response.data;


				},	(response)=>{

					vm.alerts.push({type:'danger',msg: 'Error : '+ response });

				});
		}



		vm.fetchFromServer();
		



	}

	module.exports = JobCategoryController;

/***/ },
/* 6 */
/***/ function(module, exports) {

	'use strict';




	function JobSubCategoryController($http,Config,$scope,cfpLoadingBar){
		
		
		//hard Config 
		$http.defaults.headers.post["Content-Type"] = "application/json";



		let vm = this ;

		vm.keyword = "";

		vm.IsEditMode = false ;

		vm.jobsubcategory =  null;

		vm.dtPageSize = 5 ;

		vm.dtCurrentPage = 1 ;

		vm.jobsubcategoryCollection = null;

		vm.jobcategorySelectList = null;

		vm.jobcategorySelected = null ;

		vm.alerts = [];	



		vm.IsViewDetailMode = false;
		vm.viewDetail = null;




		vm.switchToViewDetail = (item)=>{

			vm.IsEditMode = false ;
			vm.IsViewDetailMode = true;
			vm.viewDetail = item ;
			
		}

		vm.swtichToEditMode =  ()=>{

			vm.IsEditMode = true ;
			vm.IsViewDetailMode = false;

		}

		vm.swtichToViewMode = () =>{

			vm.IsEditMode = false ;
			vm.jobsubcategory = null;
			vm.IsViewDetailMode = false;
		}


		vm.remove = (item)=>{

			$http.post(Config.ServiceUrl + "jobsubcategoryservice/remove" , { "id" :  item.category_sub_job_id } )
			.then(
				(response)=>{

					vm.alerts.push({type:'success' ,msg: 'Success JobSubCategory : '+item.category_sub_job_name +' has removed .'});
					vm.fetchFromServer();
					vm.swtichToViewMode();

					

			},  (response)=>{

					vm.alerts.push({type:'danger',msg: 'Error : '+ response });

			});

		}

		vm.closeAlert = (index)=>{

			vm.alerts.splice(index, 1);

		}

		vm.update = (item)=>{
			
			vm.swtichToEditMode();


			angular.forEach(vm.jobcategorySelectList,(jobcategory)=>{

				if (jobcategory.category_job_id == item.category_job_id) {
					vm.jobcategorySelected = jobcategory;
				} 

			});


			vm.jobsubcategory = {
				"id" : item.category_sub_job_id,
				"name"  : item.category_sub_job_name
			};

			

		}


		vm.save = ()=>{

			if(vm.jobsubcategory.id){

				vm.jobsubcategory.job_id = vm.jobcategorySelected.category_job_id;

				$http.post(Config.ServiceUrl + "jobsubcategoryservice/update" , vm.jobsubcategory )
				.then(
					(response)=>{

						vm.alerts.push({type:'success',msg: 'Success JobSubCategory : has been updated .'});
						vm.swtichToViewMode();
						vm.fetchFromServer();

				},  (response)=>{

					vm.alerts.push({type:'danger',msg: 'Error : '+ response });

				});


			}else{

				vm.jobsubcategory.job_id = vm.jobcategorySelected.category_job_id;

				$http.post(Config.ServiceUrl + "jobsubcategoryservice/add" , vm.jobsubcategory )
				.then(
					(response)=>{

						vm.alerts.push({type:'success',msg: 'Success JobSubCategory : '+ vm.jobsubcategory.name +' has been added .'});
						vm.swtichToViewMode();
						vm.fetchFromServer();

				},  (response)=>{

					vm.alerts.push({type:'danger',msg: 'Error : '+ response });

				});

			}
			

		}



		vm.loadJobCategoty = ()=>{

			$http.get(Config.ServiceUrl + "jobsubcategoryservice/selectcategory")
				.then(
					(response)=>{
					
					vm.jobcategorySelectList = response.data;			

				},	(response)=>{

					vm.alerts.push({type:'danger',msg: 'Error : '+ response });

				});
		}


		vm.fetchFromServer =  ()=>{

			 //cfpLoadingBar.start();

			$http.get(Config.ServiceUrl + "jobsubcategoryservice/search")
				.then(
					(response)=>{

					vm.jobsubcategoryCollection = response.data;


				},	(response)=>{

					vm.alerts.push({type:'danger',msg: 'Error : '+ response });

				});

		}





		vm.fetchFromServer();
		vm.loadJobCategoty();




	}

	module.exports = JobSubCategoryController;

/***/ },
/* 7 */
/***/ function(module, exports) {

	'use strict';




	function AccountManageController($http,Config,$scope){
		
		
		//hard Config 
		$http.defaults.headers.post["Content-Type"] = "application/json";



		var vm = this ;

		vm.keyword = "";

		vm.IsEditMode = false ;

		vm.account =  null;

		vm.dtPageSize = 5 ;

		vm.dtCurrentPage = 1 ;

		vm.accountCollection = null;

		vm.alerts = [];

		vm.IsViewDetailMode = false;
		vm.viewDetail = null;


		vm.switchToViewDetail = (item)=>{

			vm.IsEditMode = false ;
			vm.IsViewDetailMode = true;
			vm.viewDetail = item ;
			
		}
		

		vm.swtichToEditMode = function (){

			vm.IsEditMode = true ;
			vm.IsViewDetailMode = false;

		}

		vm.swtichToViewMode = function(){

			vm.IsEditMode = false ;
			vm.account = null;
			vm.IsViewDetailMode = false;
		}


		vm.remove = (item)=>{

			$http.post(Config.ServiceUrl + "accountmanageservice/remove" , { "id" :  item.account_id } )
			.then(
				(response)=>{

					vm.alerts.push({type:'success' ,msg: 'Success Account : '+item.firstname +' '+item.lastname+' has removed .'});
					vm.fetchFromServer();
					vm.swtichToViewMode();

					

			},  (response)=>{

					vm.alerts.push({type:'danger',msg: 'Error : '+ response });
			});

		}

		vm.closeAlert = (index)=>{

			vm.alerts.splice(index, 1);

		}

		vm.update = (item)=>{
			
			vm.swtichToEditMode();

			vm.account = {
				"id" : item.account_id,
				"stdid" : item.account_std_id,
				"password" : item.password,
				"fname"  : item.firstname,
				"lname"  : item.lastname,
				"gender"  : item.gender,
				"email"  : item.email,
				"phoneno"  : item.phone_no

			};

		
		}

		vm.save = ()=>{

			if(vm.account.id){

				$http.post(Config.ServiceUrl + "accountmanageservice/update" , vm.account )
				.then(
					(response)=>{
						vm.alerts.push({type:'success',msg: 'Success Account : has been updated .'});
						vm.swtichToViewMode();
						vm.fetchFromServer();

				},  (response)=>{

						vm.alerts.push({type:'danger',msg: 'Error : '+ response });

				});

			}else{

				$http.post(Config.ServiceUrl + "accountmanageservice/add" , vm.account )
				.then(
					(response)=>{
						vm.alerts.push({type:'success',msg: 'Success Account : '+ vm.account.fname +' '+vm.account.lname +' has been added .'});
						vm.swtichToViewMode();
						vm.fetchFromServer();

				},  (response)=>{

						vm.alerts.push({type:'danger',msg: 'Error : '+ response });

				});
			}
			
		}



		vm.fetchFromServer =  ()=>{

			$http.get(Config.ServiceUrl + "accountmanageservice/search")
				.then(
					(response)=>{

					vm.accountCollection = response.data;


				},	(response)=>{

					alert(response.statusText);

				});


		}

		vm.fetchFromServer();

	}

	module.exports = AccountManageController;

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var angular = __webpack_require__(2);

	angular.module('easyCoop-backend')
		.config(__webpack_require__(9))
		.config(__webpack_require__(10))
		.config(__webpack_require__(11));




/***/ },
/* 9 */
/***/ function(module, exports) {

	
	'use strict';



	function RouteConfig($stateProvider, $urlRouterProvider,Config){


		$urlRouterProvider.otherwise("/test");

		$stateProvider
			.state('test',{
					url:'/test',
					templateUrl:  Config.ViewsUrl + 'default.html',
					controller: 'TestController',
					controllerAs: 'vm'
				})
			.state('jobcategory',{
					url:'/jobcategory',
					templateUrl:  Config.ViewsUrl + 'view.jobcategory.html',
					controller: 'JobCategoryController',
					controllerAs: 'vm'
				})
			.state('jobsubcategory',{
					url:'/jobsubcategory',
					templateUrl:  Config.ViewsUrl + 'view.jobsubcategory.html',
					controller: 'JobSubCategoryController',
					controllerAs: 'vm'
				})
			.state('accountmanage',{
					url:'/accountmanage',
					templateUrl:  Config.ViewsUrl + 'view.accountmanage.html',
					controller: 'AccountManageController',
					controllerAs: 'vm'
				});
			




	}


	module.exports = RouteConfig;

/***/ },
/* 10 */
/***/ function(module, exports) {

	


	'use strict';


	function PaginationConfig(paginationTemplateProvider,Config){

		paginationTemplateProvider.setPath(Config.ViewsUrl+ 'dirPagination.tpl.html');

	}


	module.exports = PaginationConfig;

/***/ },
/* 11 */
/***/ function(module, exports) {

	


	'use strict';


	function PluginConfig(cfpLoadingBarProvider,Config){

	 cfpLoadingBarProvider.includeSpinner = true;
	 cfpLoadingBarProvider.loadingBarInterceptor = true;
	 cfpLoadingBarProvider.latencyThreshold = 50;
		
	/*
		cfpLoadingBarProvider.includeSpinner = true;
		cfpLoadingBarProvider.includeBar = true;
		cfpLoadingBarProvider.latencyThreshold = 500;*/
		//cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
	    //cfpLoadingBarProvider.spinnerTemplate = '<div><span class="fa fa-spinner">Custom Loading Message...</div>';
	}

	module.exports = PluginConfig;

/***/ }
]);