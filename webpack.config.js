var webpack = require('webpack');

module.exports = {
    context: __dirname + '/ngbackend',
    entry: {
        app: './app.js',
        vendor: ['angular']  
    },
    debug:true,
    output: {
        path: __dirname + '/ngbackend/js',
        filename: 'app.bundle.js'
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin(/* chunkName= */"vendor", /* filename= */"vendor.bundle.js"),
        new webpack.OldWatchingPlugin()
    ],
    
};
